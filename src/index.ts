/* import World from './world'

const root = document.getElementById('root')
const world = new World('Hello')
world.sayHello(root)
 */

//基本の型定義
/* import {primitiveSample, notExistSample, anySample, unKnownSample} from "./basic"

primitiveSample()
notExistSample()
anySample()
unKnownSample() */

//関数の型定義
/* import {logMessage,logMessage2,logMessage3,logMessage4} from "./function/basic"
import {isUserSignedIn,isUserSignedIn2, sumProductsPrice} from "./function/parameters"

logMessage("Hello TypeScript")
logMessage2("Hello TypeScript2")
logMessage3("Hello TypeScript3")
logMessage4("Hello TypeScript4")

isUserSignedIn("ABC","Tsubasa")
isUserSignedIn("DEF")

isUserSignedIn2("ABC")
isUserSignedIn2("DEF")

const sum = sumProductsPrice(100,200,300,400,500)
console.log("Function parameters sample5" ,sum) */

//オブジェクト
/* import objectSample from "./object/object"
import typeAliasSample from "./object/alias"

objectSample()
typeAliasSample() */

/* import arraySample from "./array/array"
import tupleSample from "./array/tuple"
arraySample()
tupleSample() */

/* import genericsSample from "./generics/basic"
import genericAdvancedSample from "./generics/advanced"
genericsSample()
genericAdvancedSample() */

//非同期
/* import callbackSample from "./asynchrronous/callback" */
/* import promiseSample from "./asynchrronous/promise" */
/* callbackSample() */
/* promiseSample() */

import asyncAwaitSample from "./asynchrronous/asyncAwait"
asyncAwaitSample()