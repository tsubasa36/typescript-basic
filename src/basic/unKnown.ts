export default function unKnownSample() {

    const maybeNumber : unknown = 10
    console.log("unKnown sample1:", typeof maybeNumber, maybeNumber)

    const isFoo = maybeNumber === "foo"
    console.log("unKnown sample2:", typeof isFoo, isFoo)

    if (typeof maybeNumber === "number") {
        const sum = maybeNumber + 10
        console.log("unKnown sample3:", typeof sum, sum)

    }
}