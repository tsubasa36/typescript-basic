export default function arraySample() {
    //シンプルな配列の型定義
    const colors: string[] = ["red", "blue"]
    colors.push("yellow")
   /*  colors.push(123) */ //プッシュ出来ない

   console.log("array sample:1", colors)

   const even: Array<number> = [2,4,6]
   even.push(8)
   /* even.push("19") */ //文字列は受け付けない
   console.log("array sample:2", even)

   const ids: (string | number)[] = ["ABC" , 123]
   ids.push("DEF")
   ids.push(456)
   console.log("array sample:3", ids)

   //配列の型推論
   const genarateSomeArray = () => {
       const _someArray = [] //any
       _someArray.push(123) //number推論
       _someArray.push("ABC") //(string | number)[]推論
       return _someArray
   }

   const someArray = genarateSomeArray()
   /* someArray.push(true) */
   console.log("array sample:4", someArray)

   //イミュータブルな配列
   const commands: readonly string[] = ["git add", "git commit" ,"git push"]
   /* commands.push("git pull") */
   /* commands[2] = "git pull" */
   console.log("array sample:5", commands)
 }