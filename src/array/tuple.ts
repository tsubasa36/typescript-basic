export default function tupleSample() {
    //一般的なタプルの型定義
    let response: [number, string] =[200, "OK"]
    /* response = [400, "Bad", "Email parameter is missing"] */
    /* response = ["400", "Bad request"] */
    console.log("tuple ample :1", response)

    //可変長引数を使ったタプル
    const girlFriends: [string, ...string[]] = ["Miku", "Risa", "Kaede"]
    girlFriends.push("Yuki")
    console.log("tuple tample :2", girlFriends)
　}