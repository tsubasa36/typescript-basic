export default function objectSample() {
    const a :object = {
        name: "Tsubasa",
        age: 26
    }
    /* a.name */

    let country: {
        language:string
        name:string
    } = {
        language: "Japanese",
        name: "Japan"
    }

    console.log("Object sample: 1",country)

    country =  {
        language: "English",
        name: "United state of America"
    }

    console.log("Object sample: 2",country)

    //オプショナルとreadonly
    const Tsubasa: {
        age: number
        lastName: string
        readonly firstName: string
        gender?: string
    } = {
        age: 26,
        lastName: "Suzuki",
        firstName: "Tsubasa",
    }
    Tsubasa.gender = "male"
    Tsubasa.lastName = "Tanaka"

    console.log("Object sample: 3",Tsubasa)

    //インデックスシグネチャー
    const capitals: {
        [countryName:string]:string
    } = {
        Japan: "Tokyo",
        Korea: "Seoul"
    }

    capitals.China = "Beijing"
    capitals.Canada = "Ottawa"

    console.log("Object sample: 4",capitals)
}

