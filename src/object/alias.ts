export default function typeAliasSample() {
    //型エイリアス
    type country = {
        capital: string
        language: string
        name: string
    }

    const japan: country = {
        capital: "Tokyo",
        language: "Japanese",
        name: "Japan"
    }
    console.log("Object alias sample:1", japan)

    const america: country = {
        capital: "DC",
        language: "English",
        name: "USA"
    }
    console.log("Object alias sample:2", america)

    //合併型(Union)と交差型(intersection)
    type Knight = {
        hp: number 
        sp: number
        weapon: string
        swordSkill: string
    }

    type Wizard = {
        hp: number
        mp: number
        weapon: string
        magicSkill: string
    }

    //合併型... KnightまたはWizardの型を持ちます
    type Adventure = Knight | Wizard

    //濃さ型... KnightかつWizardの型を持つ
    type Paradin = Knight & Wizard

    //Knightよりの冒険者
    const Adventure1: Adventure = {
        hp: 100,
        sp: 30,
        weapon: "木の剣",
        swordSkill: "三連斬り"
    }

    //Wizardよりの冒険者
    const Adventure2: Adventure =  {
        hp:50,
        mp:100,
        weapon: "魔法の杖",
        magicSkill: "バフ"
    }

    console.log("Object alias sample:3", Adventure1)
    console.log("Object alias sample:4", Adventure2)

    const Adventure3: Paradin = {
        hp:300,
        sp:100,
        mp:100,
        weapon:"銀の剣",
        swordSkill: "三連斬り",
        magicSkill: "バフ"
    }

    console.log("Object alias sample:5", Adventure3)
}