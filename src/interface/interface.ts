interface Bread {
    calories: number
}

interface Bread {
    type: string
}

const francePan: Bread = {
    calories: 300,
    type: "hard"
}

//型エイリアス
type MaboDofu = {
    calories:number
    spicyLevel: number
}

type Rice = {
    calories: number
    gram:number
}

type MarboDon = MaboDofu & Rice //交差型
const mabodon: MarboDon ={
    calories:500,
    spicyLevel:5,
    gram:500

}

//インターフェースの継承
interface Book {
    page: number
    title: string
}

interface  Magazin extends Book {
    cycle : "daily" | "weekly" | "monthly"
}

const jump: Magazin = {
    page:300,
    title:"週刊少年ジャンプ",
    cycle:"weekly"
}

type BookType = {
    page:number
    title:string
}

interface HandBook extends BookType {
     theme:String
}

const cotrip:HandBook = {
    page:120,
    title:"小鳥っぷ",
    theme:"旅行"
}

//classでinterface
class Comic implements Book {
    page:number
    title:string

    constructor(page:number,  title:string, private publishYear:string) {
        this.page = page
        this.title = title
    }

    getPublishYear() {
        return this.title +  "が発売されたのは" + this.getPublishYear + "年です"
    }
}

const popularComic = new Comic(200,"鬼滅の刃","2016")
console.log(popularComic.getPublishYear())