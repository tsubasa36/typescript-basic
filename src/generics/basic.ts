export default function genericsSample() {
    //ジェネリック型を使わない場合
    const stringReduce = (array:string[], initialValue: string):string => {
        let result = initialValue
        for(let i=0; i < array.length; i++) {
            result += array[i]
        }
        return result
    }
    console.log("Generics sample:1",stringReduce(["May", "the","force", "be", "with", "you" ],""))

    const numberReduce = (array:number[], initialValue: number):number => {
        let result = initialValue
        for(let i=0; i < array.length; i++) {
            result += array[i]
        }
        return result
    }
    console.log("Generics sample:2",numberReduce([100,200,300],1000))

    type reduce = {
        (array:string[],initialValue:string): string
        (array:number[],initialValue:number): number
    }

    type genericReduce<T> = {
        (array:T[], initialValue:T) :T
    }

    const genericStringReduce: genericReduce<string> = (array, initialValue) => {
        let result = initialValue
        for(let i=0; i < array.length; i++) {
            result += array[i]
        }
        return result
    }
    console.log("Generics sample:3",genericStringReduce(["Make","TYPESCRIPT" ],""))

    const genericNumberReduce: genericReduce<number> = (array, initialValue) => {
        let result = initialValue
        for(let i=0; i < array.length; i++) {
            result += array[i]
        }
        return result
    }
    console.log("Generics sample:4",genericNumberReduce([-100,-200,-300],1000))

    //いろいろなジェネリック型の定義方法
    //完全な呼び出しシグネチャ
    type genericReduce2 = {
        <T>(array:T[], initialValue:T): T
        <U>(array:U[], initialValue:U): U
    }

    //呼び出しシグネチャの省略
    type genericReduce3<T> = (array:T[], initialValue:T) => T
    type genericReduce4 = <T>(array:T[], initialValue:T) => T
}