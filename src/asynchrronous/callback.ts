export default function callbackSample() {
    const  url = "https://api.github.com/users/tsupasan36"

    //コールバックで呼び出す非道処理
    const fetchProfile = () => {
        return fetch(url)
            .then((res) => {
                //レスポンスのbodyをjsonで読み取った結果を返す
                res.json()
                    .then((json) => {
                        console.log("asynchronous callback sample:1", json)
                        return json
                    })
                    .catch((error) => {
                        console.error(error)
                    })
            })
            .catch((error) => {
                console.error(error)
            })
    }

    const profile = fetchProfile()
    console.log("asynchronous callback sample:2", profile)
}